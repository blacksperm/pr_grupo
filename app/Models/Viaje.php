<?php 
namespace App\Models;
use App\Entities\EntViaje;
use CodeIgniter\Model;




class Viaje extends Model{
  // protected $table      = 'viaje';
  // protected $primaryKey = 'id_viaje';

  // protected $returnType = 'App\Entities\EntViaje';
 //    protected $useSoftDeletes = true;

 //    protected $allowedFields = ['id_viaje', 'id_cliente', 'duracion', 'distancia', 'punto_partida','punto_final','tarifa'];

 //    protected $useTimestamps = false;

    // protected $validationRules    = [];
    // protected $validationMessages = [];
    // protected $skipValidation     = true;

  public function getViajes(){
     $x = $this->query('Call sp_consulta()');
     $res  = array();
     foreach ($x->getResult() as $v) {

         $x1 = new EntViaje();
     
           $x1->setHora($v->hora_inicial);
         $x1->setEstado($v->estado_viaje);
         $x1->setFec($v->fecha);
         $x1->setNcliente($v->nombre_cliente);
         $x1->setViajes($v->id_viaje);
         $x1->setClientes($v->id_cliente);
         $x1->setDuracion($v->duracion);
         $x1->setPartida($v->punto_partida);
         $x1->setFinal($v->punto_final);
         $x1->setTarifa($v->tarifa);
         array_push($res, $x1);

         
     }

     return $res;
 }


}